# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 10:54:59 2018

@author: ILOOKFORME102
"""

import requests
import pandas as pd
import time

GOOGLE_MAPS_API_URL = 'http://maps.googleapis.com/maps/api/geocode/json'
addresses = pd.read_csv("Employee_Addresses.csv")
list_address = []

def get_lat_long(row):
    params = {
            "address": row["address"]
            }
    response = requests.get(GOOGLE_MAPS_API_URL, params= params)
    response = response.json()
    if response['status'] == 'OK':
        
        result = response['results'][0]
        employee_address = dict()
        employee_address['lat'] = result['geometry']['location']['lat']
        employee_address['lng'] = result['geometry']['location']['lng']
        time.sleep(1)
        list_address.append(employee_address)  
      
        print(employee_address)
    
for index, row in addresses.iterrows():
    get_lat_long(row)

list_address =pd.DataFrame(list_address)
list_address.to_csv('lat_long_address.CSV')