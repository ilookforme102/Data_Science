# -*- coding: utf-8 -*-
"""
Created on Sat Aug  4 02:42:35 2018

@author: ILOOKFORME102
"""
import folium
import pandas as pd

employees = pd.read_csv('lat_long_address.csv')

bus_stop = pd.read_csv('intersection.csv')

 
# Make an empty map
m = folium.Map(location=[37.7749,122.4194], zoom_start=12)
 
# I can add marker one by one on the map
for i in range(0,len(employees)):
    folium.CircleMarker([employees.iloc[i]['lng'], employees.iloc[i]['lat']]).add_to(m)
for i in range(0,len(bus_stop)):
    folium.Marker([bus_stop.iloc[i]['lng'], bus_stop.iloc[i]['lat']], radius=6, fill_color='red',color='red', fill_opacity=1).add_to(m)
# Save it as html
m.save('bus_map.html')
